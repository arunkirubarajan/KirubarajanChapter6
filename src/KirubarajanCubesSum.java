// Arun Kirubarajan - determines sum of cubes of a number and all numbers that are equal to their digits' cubes' sum

import java.util.Scanner;

public class KirubarajanCubesSum {

	public static void main(String[] args) {
	
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter non-zero integer: ");
		String number = input.next();
				
		System.out.println("The sum of the digits' cubes is " + cubesSum(number) + ".");
		
		// if cube is equal to its sums
		
		if (Integer.parseInt(number) == cubesSum(number)) {
			
			System.out.println(number + " is equal to the sum of the cubes of its digits.");
			
		}
		
		System.out.println("Two, three and four digit numbers that are equal to the sum of the cubes of their digits: ");
		
		// iterates over every number between 10 and 9999 (all two digit, three digit and four digit numbers)
		
		for (int j = 10; j <= 9999; j++) {
			
			if (j == cubesSum(Integer.toString(j))) {
				
				System.out.println(j + " is equal to the sum of the cubes of its digits.");
				
			}
			
		}

	}
	
	static int cubesSum (String number) {
		
		// converts each digit of string of given integer to single integer and adds cubes together
	
		int sum = 0;
		
		for (int i = 0; i < number.length(); i++) {
			
			int digit = Character.getNumericValue(number.charAt(i));
			
			sum = sum + (digit * digit * digit);
			
		}
		
		return sum;
	
	}

}
