// Arun Kirubarajan - determines if two given numbers are prime and outputs every prime between them

import java.util.Scanner;

class KirubarajanPrimeNumber {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("What is the first integer?");
		int firstNumber = input.nextInt();
		
		System.out.println("What is the second integer?");
		int secondNumber = input.nextInt();
		
		// checking if given numbers are divisible by primes less than square root of 100 (maximum output) and flagging if no remainder (signifying a composite number)
		
		boolean firstFlag = isPrime(firstNumber) ? true : false, secondFlag = isPrime(secondNumber) ? true : false;

		// displaying if given numbers are prime or composite
		
		if (firstFlag == true) {
			
			System.out.println("The first number is a prime number.");
			
		}
		
		else {
			
			System.out.println("The first number is not a prime number.");
			
		}

		
		if (secondFlag == true) {
			
			System.out.println("The second number is a prime number.");
			
		}
		
		else {
			
			System.out.println("The second number is not a prime number.");
			
		}
		
		System.out.println("Calculating primes between two given numbers: ");
		
		// iterating over numbers between two given primes and checking if primes
		
		boolean composite = true;
		
		if (firstNumber < secondNumber) {

			for (int j = firstNumber + 1; j <= secondNumber; j++) {
				
				if (isPrime(j)) {
					
					System.out.println(j);
					composite = false;
					
				}
				
			}
			
		}
		
		else if (firstNumber > secondNumber) {
			
			for (int j = secondNumber + 1; j <= firstNumber; j++) {
				
				if (isPrime(j)) {
					
					System.out.println(j);
					composite = false;
					
				}
				
			}
			
		}
		
		else if (firstNumber == secondNumber) {
			
			System.out.println("There are no primes between the two numbers");
			
		}
		
		// if no prime numbers between two given numbers
		
		if (composite) {
			
			System.out.println("There are no prime numbers between the two given integers.");
		
		}
		
	}
	
	static boolean isPrime (int number) {
		
		// don't hate me - this is the fastest way to verify primes for integers from 1 to 100 :)
		
		int[] primes = {2, 3, 5, 7};
		
		for (int i = 0; i < primes.length; i++) {
			
			if (number % primes[i] == 0 && number != primes[i]) {

				return false;
				
			}

		}
		
		return true;
		
	}

}
