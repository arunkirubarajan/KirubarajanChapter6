// Arun Kirubarajan - the iconic guessing game

import java.util.Scanner;
import java.util.Random;

public class KirubarajanGuessingGame {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		Random random = new Random();
		
		// generates random number between 1 and 20
		
		int number = random.nextInt(20) + 1;  
		
		// checks guess with answer and flags if correct to break out of loop
		
		boolean flag = false;
		
		do {
			
			System.out.println("Enter your guess: ");
			int answer = input.nextInt();
			
			if (answer == number) {
				
				System.out.println("You got it!");
				flag = true;
				
			}
			
			else {
				
				System.out.println("Try again.");
				
			}
			
		}
		while (flag == false);
		
	}

}
