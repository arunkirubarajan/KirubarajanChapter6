// Arun Kirubarajan - determines all prime factors of given number

import java.util.Scanner;

public class KirubarajanPrimeFactors {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		System.out.println("Enter an integer between 1 and 100: ");
		int number = input.nextInt();
		
		System.out.println("The prime factors are: ");
		
		int[] primes = {2, 3, 5, 7};
		
		boolean flag = false;
		
		while (flag == false) {
			
			flag = false;
			
			boolean prime = true;
			
			for (int i = 0; i < 4; i++) {
				
				if (number % primes[i] == 0) {
					
					number = number / primes[i];
					prime = false;
					
					System.out.println(primes[i]);
					
				}
				
			}
			
			if (number == 1 || prime == true) {
				
				flag = true;
				
			}
			
		}
		
		if (number != 1) {
			
			System.out.println(number);
			
		}
		
	}

}
