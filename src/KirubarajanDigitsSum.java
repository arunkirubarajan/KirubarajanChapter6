// Arun Kirubarajan - calculates sum of digits of given integer

import java.util.Scanner;

public class KirubarajanDigitsSum {

	public static void main(String[] args) {
		
		Scanner input = new Scanner (System.in);
		System.out.println("What is the integer: ");
		
		String number = input.next();
		int sum = 0;

		// takes each character of string (each digit) and convert to integer and adding to sum
		
		for (int i = 0; i < number.length(); i++) {
			
			sum = sum + Character.getNumericValue(number.charAt(i));
			
		}
		
		System.out.println("The sum of the integer's digits is " + sum + ".");
		
	}

}
