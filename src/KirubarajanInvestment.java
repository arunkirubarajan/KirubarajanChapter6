// Arun Kirubarajan - determines amount of time for 2500 to double at 7.5% compounded annually

import java.util.Scanner;

public class KirubarajanInvestment {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		// using rearranged future value formula to solve for time
		
		double time = Math.log10(2) / Math.log10(1.075);
		
		// determines nearest whole year (rounding up)
		
		int nearestTime = (int) (time + (1 - (time % 1)));
		 
		System.out.println("It will take " + nearestTime + " (" + time + " to be precise) years for $2500 to be worth $7500 at 7.5% compounded annaully.");

	}

}
